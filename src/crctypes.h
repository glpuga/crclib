/*
 * crctypes.h
 *
 *  Created on: Jun 30, 2016
 *      Author: glpuga
 */

#ifndef CRCTYPES_H_
#define CRCTYPES_H_

#include <stdint.h>

typedef uint32_t crc32type;
typedef uint16_t crc16type;

#endif /* CRCTYPES_H_ */
