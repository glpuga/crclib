/*
 ============================================================================
 Name        : CRCCALCULATOR.c
 Author      : GLP
 Version     :
 Copyright   : 
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "crclib.h"
#include "crclibaux.h"

int main(int argc, char *argv[])
{
	char *testString = "123456789";

	if (argc > 1)
	{
		testString = argv[1];
	}


	printf("CRCLIB Run Executable\n");
	printf("---------------------\n");
	printf("\n");
	printf(" * Test String : %s\n", testString);
	printf("\n");
	printf(" *   CRC-CCITT : %s\n", crclibCRCToString(crclibDoCCITT(testString, strlen(testString))));
	printf(" *      CRC-32 : %s\n", crclibCRCToString(crclibDoCRC32(testString, strlen(testString))));
	printf("\n");


	return EXIT_SUCCESS;
}
