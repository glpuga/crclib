/*
 * crcparams.h
 *
 *  Created on: Jun 30, 2016
 *      Author: glpuga
 */

#ifndef CRCPARAMS_H_
#define CRCPARAMS_H_

/*
 * CRC-CCITT
 */

#define CRC_CCITT_ORDER 16
#define CRC_CCITT_POLY  0x1021

/* Si bien el valor inicial del polinomio se encuentra perfectamente definido
 * que es 0xffff, no se encuentra igualmente normalizado a qué estructura de
 * cálculo del CRC corresponde este valor inicial (modo directo o indirecto,
 * o alternativamente con o sin finalización explícita con ceros, ver discusión del
 * tema en http://srecord.sourceforge.net/crc16-ccitt.html ).
 *
 * La diferencia entre una y otra estructura puede salvarse mediante la utilización
 * de un valor inicial diferente.
 */

#define CRC_CCITT_START 0xFFFF
// #define CRC_CCITT_START 0x1D0F


/*
 * CRC32
 */

#define CRC_CRC32_ORDER         32
#define CRC_CRC32_POLY          0x04C11DB7
#define CRC_CRC32_REVERSE_POLY  0xEDB88320
#define CRC_CRC32_START         0xFFFFFFFF
#define CRC_CRC32_REVERSE_START 0xFFFFFFFF
#define CRC_CRC32_FINALXOR      0xFFFFFFFF

#endif /* CRCPARAMS_H_ */
