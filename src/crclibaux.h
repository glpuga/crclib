/*
 * crclibaux.h
 *
 *  Created on: Jun 30, 2016
 *      Author: glpuga
 */

#ifndef CRCLIBAUX_H_
#define CRCLIBAUX_H_

#include "crctypes.h"

const char *crclibCRCToString(uint32_t crc);

void crclibGenerateCCITTFastTable();

void crclibGenerateCRC32FastTable();

#endif /* CRCLIBAUX_H_ */
