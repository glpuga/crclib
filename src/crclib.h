/*
 * crclib.h
 *
 *  Created on: Jun 30, 2016
 *      Author: glpuga
 */

#ifndef CRCLIB_H_
#define CRCLIB_H_

#include "crctypes.h"

crc32type crclibDoCRC32(void *data, int32_t len);

crc16type crclibDoCCITT(void *data, int32_t len);

#endif /* CRCLIB_H_ */
