/*
 * crclib.c
 *
 *  Created on: Jun 30, 2016
 *      Author: glpuga
 */

#include <stdio.h>
#include "crclibaux.h"
#include "crcparams.h"

#define BUFFER_LEN 20

char stringBuffer[BUFFER_LEN];

const char *crclibCRCToString(uint32_t crc)
{
	sprintf(stringBuffer, "0x%4.4x", crc);

	return stringBuffer;
}

void crclibGenerateCCITTFastTable()
{
	crc16type crcRegister;
	int32_t tableIndex;
	int32_t i;

	for (tableIndex = 0; tableIndex < 256; tableIndex++)
	{
		crcRegister = ((crc16type)tableIndex) << 8;

		for (i = 0; i < 8; i++)
		{
			if (crcRegister & (1 << (CRC_CCITT_ORDER - 1)))
			{
				crcRegister = (crcRegister << 1) ^ CRC_CCITT_POLY;
			} else {
				crcRegister = crcRegister << 1;
			}
		}

		printf("0x%4.4X,\n", crcRegister);
	}
}

void crclibGenerateCRC32FastTable()
{
	crc32type crcRegister;
	int32_t tableIndex;
	int32_t i;

	for (tableIndex = 0; tableIndex < 256; tableIndex++)
	{
		crcRegister = (crc32type)tableIndex;

		for (i = 0; i < 8; i++)
		{
			if (crcRegister & 1)
			{
				crcRegister = (crcRegister >> 1) ^ CRC_CRC32_REVERSE_POLY;
			} else {
				crcRegister = (crcRegister >> 1);
			}
		}

		printf("0x%8.8X,\n", crcRegister);
	}
}

